cc.Class({
    extends: cc.Component,

    properties: {

        pointerButton:{
            default: null,
            type: cc.Node
        },

        result:{
            default: null,
            type: cc.Label
        },
        awardNum: 8,
        rotateTime: 6.0,
        circleNum: 5,
    },

    onLoad () {
        this.IsSpin = false;
        
    },

    onClickLucky() {
        console.log("Start");
        if(this.IsSpin){
            return;
        }
        this.IsSpin = true;    
            
        this.currentAngle = 0 ;
        if(this.pointerButton.rotation != 0){

            this.currentAngle = this.pointerButton.rotation + (360 * this.circleNum);
            this.pointerButton.rotation = this.currentAngle;
        }
        
        var random_awards = Math.floor(Math.random()* this.awardNum+1);
        //this commant only use for check, REMEMBER to delete
        console.log("Result is ====>>> =", random_awards);
        // rotate angle
        this.rotationNum = (random_awards) * (360 / this.awardNum) + this.currentAngle;
        // left right angle
        // this.random_1 = Math.floor(Math.random()* 45 - 22.5);
        this.random_1 = Math.floor(Math.random()* (360 / this.awardNum));

        let action = this.DialRotateAction(this.circleNum, this.random_1);
        this.pointerButton.runAction(action);

        // let self = this;
        // this.scheduleOnce(function () {
        //     let action_1 = cc.sequence(
        //         cc.rotateBy(0.2, 0), //wait awhile
        //         cc.rotateBy(1.5, self.random_1 / -1),  // pointer in the middle
        //     ).easing(cc.easeInOut(3.0));
        //     self.pointerButton.runAction(action_1);
        // }, this.rotateTime);

        // This scheduleOnce is set for show delay
        this.scheduleOnce(function(){
            this.showResult(random_awards);
            this.IsSpin = false;
            console.log("wheel rotation is "+this.pointerButton.rotation);
        },this.rotateTime); 
    },

    // rotate direction
    DialRotateAction:function(circleNum, additionalAngle) {
        let self = this;
        let action = cc.rotateBy(self.rotateTime, 360 * circleNum + self.rotationNum +additionalAngle).easing(cc.easeInOut(3.0));
        return action;
    },

    showResult(res){
        switch(res) {
            case 8:
            //   prize = "img/win-01.png";//小黑糖奶
            console.log(8);
            this.result.string = "十元折價卷";
            break;
            case 7:
            //   prize = "img/win-02.png";//小金萱
            console.log(7);
            this.result.string  = "小黑糖奶";
            break;
            case 6:
            //   prize = "img/win-05.png";//整筆半價
            console.log(6);
            this.result.string  = "小金萱";
            break;
            case 5:
            //   prize = "img/win-04.png";//買一送一
            console.log(5);
            this.result.string  = "整筆半價";
            break;
            case 4:
            //   prize = "img/win-01.png";//小黑糖奶
            console.log(4);
            this.result.string  = "買一送一";
            break;
            case 3:
            //   prize = "img/win-02.png";//小金萱
            console.log(3);
            this.result.string  = "小黑糖奶";
            break;
            case 2:
            //   prize = "img/win-06.png";//銘謝惠顧
            console.log(2);
            this.result.string  = "小金萱";
            break;
            case 1:
            //   prize = "img/win-03.png";//十元折價卷
            console.log(1);
            this.result.string  = "銘謝惠顧";
            break;
        }
    },

    // reset roller
    onClickRefreshRotate(){
        console.log("reset");
        if(this.pointerButton.rotation == 0) return;

        if(this.pointerButton.rotation != 0){
            this.pointerButton.rotation = 0;
        }
    }
});
