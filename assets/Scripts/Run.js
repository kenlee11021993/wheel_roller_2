cc.Class({
    extends: cc.Component,

    properties: {
        buttonStart:{
            default: null,
            type: cc.Button
        },

        pointerButton:{
            default: null,
            type: cc.Node
        },

        result:{
            default: null,
            type: cc.Label
        },
        awardNum: 8,
        rotateTime: 6.0,
        circleNum: 5,
    },

    onLoad () {
        this.IsSpin = false;
        this.buttonStart.enabled = true;
    },

    onClickLucky() {
        console.log("Start");
        this.IsSpin = true;      
        this.currentAngle = 0 ;
        // condition is for reset the angle PS:(cause in cocos it will negative no same as Unity)
        if(this.pointerButton.rotation != 0){
            // console.log("ReNew BEFORE of currentAngle "+this.currentAngle)
            this.answer1 = (360 * this.circleNum);

            // console.log("Answer is  "+this.answer1);
            this.currentAngle = this.pointerButton.rotation + this.answer1;

            // console.log("ReNew of currentAngle "+this.currentAngle)
            this.pointerButton.rotation = this.currentAngle;
        }
        
        // Generate a number 1~8
        // var random_awards = Math.floor(Math.random()* this.awardNum+1);
        //this commant only use for check, REMEMBER to delete
        // console.log("Result is ====>>> =", random_awards);
        //================================================================
        var random_awards = Math.floor(Math.random()*100 +1);
        console.log("Result is ====>>> =", random_awards);
        var finalGet = this.rateNumber(random_awards);
        console.log("So the rate is ="+finalGet );
        //================================================================
        // rotate angle
        this.rotationNum = (finalGet) * (360 / this.awardNum) + this.currentAngle;
        // console.log("so the rotation number is "+this.rotationNum);
        // left right angle
        // this.random_1 = Math.floor(Math.random()* 45 - 22.5);
        // this.random_1 = Math.floor(Math.random()* (360 / this.awardNum)); // old code
        // console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        // console.log("HERE IS THE RANDOM_1 "+ this.random_1);
        let action = this.DialRotateAction(this.circleNum); // , random_awards
        this.pointerButton.runAction(action);

        // let self = this;
        // this.scheduleOnce(function () {
        //     let action_1 = cc.sequence(
        //         cc.rotateBy(0.2, 0), //wait awhile
        //         cc.rotateBy(1.5, self.random_1 / -1),  // pointer in the middle <<<
        //     ).easing(cc.easeInOut(3.0));
        //     self.pointerButton.runAction(action_1);
        // }, this.rotateTime);

        // This scheduleOnce is set for show delay
        this.scheduleOnce(function(){
            this.showResult(finalGet);
            this.IsSpin = false;
            this.buttonStart.enabled = true;
            // console.log("wheel rotation is "+this.pointerButton.rotation);
        },this.rotateTime); 
    },

    // rotate direction
    DialRotateAction:function(circleNum, additionalAngle) {
        let self = this;                 // additionalAngle (random_awards) can skip to add parameter in this function +additionalAngle
        let action = cc.rotateBy(self.rotateTime, (360 * circleNum) + self.rotationNum).easing(cc.easeInOut(3.0));
        return action;
    },

    showResult(res){
        switch(res) {
            case 8:
            //   prize = "img/win-01.png";//小黑糖奶
            console.log(8);
            this.result.string = "烧烤";
            break;
            case 7:
            //   prize = "img/win-02.png";//小金萱
            console.log(7);
            this.result.string  = "火锅";
            break;
            case 6:
            //   prize = "img/win-05.png";//整筆半價
            console.log(6);
            this.result.string  = "MAMAK";
            break;
            case 5:
            //   prize = "img/win-04.png";//買一送一
            console.log(5);
            this.result.string  = "再转";
            break;
            case 4:
            //   prize = "img/win-01.png";//小黑糖奶
            console.log(4);
            this.result.string  = "中餐";
            break;
            case 3:
            //   prize = "img/win-02.png";//小金萱
            console.log(3);
            this.result.string  = "西餐";
            break;
            case 2:
            //   prize = "img/win-06.png";//銘謝惠顧
            console.log(2);
            this.result.string  = "日本餐";
            break;
            case 1:
            //   prize = "img/win-03.png";//十元折價卷
            console.log(1);
            this.result.string  = "快餐";
            break;
        }
    },

    // showResult(res){
    //     switch(res) {
    //         case 8:
    //         //   prize = "img/win-01.png";//小黑糖奶
    //         console.log(8);
    //         this.result.string = "十元折價卷";
    //         break;
    //         case 7:
    //         //   prize = "img/win-02.png";//小金萱
    //         console.log(7);
    //         this.result.string  = "小黑糖奶";
    //         break;
    //         case 6:
    //         //   prize = "img/win-05.png";//整筆半價
    //         console.log(6);
    //         this.result.string  = "小金萱";
    //         break;
    //         case 5:
    //         //   prize = "img/win-04.png";//買一送一
    //         console.log(5);
    //         this.result.string  = "整筆半價";
    //         break;
    //         case 4:
    //         //   prize = "img/win-01.png";//小黑糖奶
    //         console.log(4);
    //         this.result.string  = "買一送一";
    //         break;
    //         case 3:
    //         //   prize = "img/win-02.png";//小金萱
    //         console.log(3);
    //         this.result.string  = "小黑糖奶";
    //         break;
    //         case 2:
    //         //   prize = "img/win-06.png";//銘謝惠顧
    //         console.log(2);
    //         this.result.string  = "小金萱";
    //         break;
    //         case 1:
    //         //   prize = "img/win-03.png";//十元折價卷
    //         console.log(1);
    //         this.result.string  = "銘謝惠顧";
    //         break;
    //     }
    // },

    
    rateNumber(rollNumber)
    {
        // 2%
        if(rollNumber <= 2)
        {
            return 8;
        }

        // 15%
        else if(rollNumber <= 17)
        {
            return 7;
        }

        // 23%
        else if(rollNumber <= 40)
        {
            return 6;
        }

        // 10%
        else if(rollNumber <= 50)
        {
            return 4;
        }

        // 15%
        else if(rollNumber <= 65)
        {
            return 3;
        }

        // 15%
        else if(rollNumber <= 80)
        {
            return 2;
        }

        // 8%
        else if(rollNumber <= 88)
        {
            return 1;
        }

        // 12%
        else
        {
            return 5;
        }
    },

    update(dt){
        if(this.IsSpin){
            this.buttonStart.enabled = false;
        }

    }
});
